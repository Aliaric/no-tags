
(function($){
  $( document ).ready(function() {
        var lutes_html_path = '/sites/exercises.tst/files/lutes.html';
        var lutes_html = $.get(lutes_html_path, function(data) {
          var lutes_text = $(data).text();
          $( "#no-tags-js-id" ).click(function() {
            $('#wait-for-showtime').css("display", "inline");
            $('#wait-for-showtime').text(''); 
            $("#wait-for-showtime").append(document.createTextNode(lutes_text));
            setTimeout(function(){
              $('#wait-for-showtime').fadeOut()
            ;}, 1000);
          });
        });
  });
})(jQuery);

